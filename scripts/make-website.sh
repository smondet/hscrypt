#! /bin/sh

usage () {
    cat >&2 <<EOF
usage: $0 <output-dir>
EOF
}

output_path="$1"
if [ "$output_path" = "" ] ; then
    usage
    exit 2
fi

css=https://smondet.gitlab.io/fa2-smartpy/style.css

mkdir -p "$output_path"

tmpindex="$(mktemp /tmp/hscrypt-website-XXX.md)"

sed 's@<file:///tmp/\([^>]*\)>@[\1](\1)@g' README.md > "$tmpindex"

pandoc --css "$css" -s "$tmpindex" -o "$output_path/index.html"

printf 'Hello\n===\n\nThis *is* a `test`\n' | pandoc -s -o /tmp/test.html
hscrypt -f /tmp/test.html -p test-pass-phrase -o "$output_path/hscrypt-test.html"
 

